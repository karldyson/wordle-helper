WORDLE
======

Introduction
------------

This is not an excerise in cheating the game. Sometimes I see games like wordle as a challenge in terms of "can I write some code to help with that" in terms of the technical challege of writing code. I'm not a programmer, and I don't code for a living, and so a bit of brain exercise occasionally is nice.

Use
---

The command line takes a number of parameters, as follows:

- a bunch of letters that cannot be in the word
- the five positions in the word with letters that you *know* are in the right place. Use dots for unknown letters.
- examples of "amber" letter, so letters you know are NOT in the position you guessed, but *are* in the word somewhere. You can repeat as many of these as you need.

Example
-------

    $ ./wordle
    Random selection: fined

The game gives us B G B B B so we know we don't have f, n, e or d and that i is in the right place:

    $ ./wordle fned .i...
    More than 24 guesses, random selection from 274 guesses is: gilts

The game gives us Y G Y Y B so we know we now additionally don't have s, and that g, l and t are in the word but not in those positions

    $ ./wordle fneds .i... g.... ..l.. ...t.
    light
